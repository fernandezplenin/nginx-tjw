# nginx-tjw

TODO:
- Test with non productive environment (currently bans on ubuntu)
- Use conf files in /etc/nginx-tjw/ instead of variables in code for os version (read it at reload.py)  and maxRequests
- Use threads to process serveral files at the same time
- Delete sample files after processing them
- Manage errors along the code, if there's an error, print to log file

Nginx Traffic Justice Warrior is a python lightweight easy to use code that scans your nginx server_names and access logs to limit the number of requests per second that a single IP can perform on the virtual hosts, if it's exceeded in one of the virtual hosts, it blocks the IP via iptables.

reload.py scans the server_names in nginx and stores the access log file paths in:
	/etc/nginx-tjw/access_logs.conf

ban.py builds samples from the nginx access log files found in /etc/nginx-tjw/access_logs.conf and if an IP exceeds the maximum requests configured per time unit (delay) it bans the IP and logs the action.

Logs:
	/var/log/nginx-tjw.log
