# Script assumes your nginx access log files is saved in /var/log/nginx/
# and they are formatted this way:
# 51.75.202.154 - - [23/May/2020:17:26:44 +0000] "POST /xmlrpc.php HTTP/1.1" 200 231 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0"
# <IP> - - [<DATE>] "<METHOD> <requested file> <PROTOCOL>" <RESPONSE> ...
# TODO
# Put threads to build the sample files simultaneously
#!/usr/bin/env python3
# Please store OS version in /etc/nginx-tjw/conf instead of checking everytime

import os
import subprocess
import datetime
import time

OS=subprocess.check_output("cat /etc/*release |grep DISTRIB_ID | awk -F '=' '{print $2}'", shell=True).decode('utf-8').replace('\n','')

if (OS=='Ubuntu'):
    iptables='/sbin/iptables'
else:
    iptables='/usr/sbin/iptables'

now= datetime.datetime.now().strftime("%d/%m/%Y %H:%M")


delay= 3 # time for sample
maxRequests = 7 # requests per sample time
logsPath= "/etc/nginx-tjw/access_logs.conf"

builtSamples= False
if os.path.isfile(logsPath):
    try:
        print("Creating samples from logs")
        with open(logsPath) as logs:
            line = logs.readline().replace('\n','')
            while line:
                #print(line)
                log= open(line, "r")
                try:
                    lastLine= log.readlines()[-1].replace('\n','')
                    #print(lastLine)

                    time.sleep(delay)
                    log2= open(line, "r")
                    lines= [l.rstrip('\n') for l in log2]
                    #print(*lines)
                    found= False
                    i=-1
                    print("Looking for: "+lastLine)
                    sample= open(line + "-sample", "w")
                    while not found:
                        print(lines[i] + "\n")
                        if (lines[i]==lastLine):
                            found= True
                            sample.write(lines[i] + "\n")
                            builtSamples= True
                            sample.close()
                        else:
                            sample.write(lines[i] + "\n")
                            i=i-1

                    log2.close()
                #if empty file
                except IndexError:
                    lastLine = 'null'

                line = logs.readline().replace('\n','')
                log.close()
    finally:
        logs.close()

    if (builtSamples):
        iptablesBanned= subprocess.check_output("iptables -S | grep INPUT | grep -i DROP | awk '{print $4}' | awk -F '/' '{print $1}'", shell=True).decode('utf-8')
        iptablesBanned= iptablesBanned.split('\n')[:-1]
        print("Banned IPs")
        print(*iptablesBanned)
        print("\nSamples built, processing nginx requests...")
        # get list of all paths to samples
        ls= subprocess.check_output("ls -1a /var/log/nginx/*sample*", shell=True).decode('utf-8')
        sampleFiles= ls.split('\n')[:-1]
        for sample in sampleFiles:
            lines= open(sample, "r")
            requests= [l.rstrip('\n') for l in lines]
            #print("\tRequests in " + sample  +  "  for " + str(delay) + " second(s)")
            ipAddresses = {}
            for r in requests:
                #print(r + "\n\t\t")
                ip = r.split()[0]
                if (ip not in ipAddresses):
                    ipAddresses[ip] = 1
                else:
                    ipAddresses[ip] = ipAddresses.get(ip, 0) + 1 # increase requests made from that IP

            for ip in ipAddresses:
                if (ipAddresses[ip] > maxRequests and ip != '127.0.0.1' and ip != 'localhost'):
                    if (ip not in iptablesBanned):
                        try:
                            subprocess.check_output(iptables + ' -I INPUT -s' + ip + '/32 -j DROP -m comment --comment "-  blocked by nginx-tjw at ' + now + '"', shell=True).decode('utf-8')
                            tjwlog=open("/var/log/nginx-tjw.log", "a+")
                            tjwlog.write("\n" + now + " Banned "+ ip + " - too much requests in " + sample  + "\n")
                            tjwlog.close()
                        except subprocess.CalledProcessError as e:
                            print(e.output)
                    else: # ip is already banned
                        tjwlog=open("/var/log/nginx-tjw.log", "a+")
                        tjwlog.write("\n" + now + " WARNING - Already Banned "+ ip + " still appears in with too much requests in " + sample  + "\n")
                        tjwlog.close()
