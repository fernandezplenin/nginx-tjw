#!/usr/bin/env python3
import os
import subprocess
import datetime

nginx_conf= "/etc/nginx/conf.d/"
nginx_enabled= "/etc/nginx/sites-enabled/"
now= datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
log= open("/var/log/nginx-tjw.log", "a+")

if not os.path.exists('/etc/nginx-tjw/'):
    os.makedirs('/etc/nginx-tjw/')
conf= open("/etc/nginx-tjw/access_logs.tmp", "w")

log.write("\n" + now + " - Reloading server_names and access log files...")
if (os.path.exists(nginx_enabled) and os.path.exists(nginx_conf)):
    search = subprocess.check_output("grep -r server_name " + nginx_conf + " " + nginx_enabled, shell=True).decode('utf-8')
    searchList= search.split(';')
    searchList= searchList[:-1] #remove element break
else:
    if os.path.exists(nginx_conf):
        search = subprocess.check_output("grep -r server_name " + nginx_conf, shell=True).decode('utf-8')
        searchList= search.split(';')
        searchList= searchList[:-1] #remove element break
    else:
        print("ERROR. Check if nginx is installed correctly. " + nginx_conf + " directory is not present.")

#print (searchList)
server_names= []
for element in searchList:
        server= {'confFile': element.split(':')[0] , 'server_name': element.split(':')[1]}
        # if path is an actually working conf file and server name isn't commented
        if (server['confFile'].endswith(".conf") and not server['server_name'].startswith('#')):
            server['confFile']= server['confFile'].replace('\n', '')
            server['server_name']= server['server_name'].split()[1]
            # search log files
            search= subprocess.check_output("cat " + server['confFile'] + " | grep access_log | grep -v 'off;' | awk '{print $2}'", shell=True).decode('utf-8')
            accessLogList= search.split(';')
            accessLogList= accessLogList[:-1]
            server['accessLogs'] = []
            for e in accessLogList:
                if (not e.startswith('#') and e not in server['accessLogs']):
                    server['accessLogs'].append(e.replace('\n', ''))
            if (server not in server_names):
                server_names.append(server)
# print, transform to string and print to file
for e in server_names:
    print (e)
    log.write('\n\tFound: ' + e['server_name'] + ' in: ' + e['confFile']  + '\n\t\tAccess log file(s): '.join(e['accessLogs']))
    for item in e['accessLogs']:
        conf.write(item + "\n")
log.write("\n")
log.close()
conf.close()
# eliminate repeated lines in conf
try:
    output= subprocess.check_output("cat /etc/nginx-tjw/access_logs.tmp |sort | uniq > /etc/nginx-tjw/access_logs.conf", shell=True)
except subprocess.CalledProcessError as e:
    print(e.output)
try:
    output2= subprocess.check_output("rm -f /etc/nginx-tjw/access_logs.tmp", shell=True)
except subprocess.CalledProcessError as e:
    print(e.output)
